#include <msp430.h>				


/**
 * 捕获验证实验
 */

// 硬件连接
// P2.1 - 作为信号输入（Timer1_A3.CCI1A）
// P1 P3 - 作为扫描数码管显示控制线，显示捕获时间值/信号周期值（ms为单位，16进制显示）

// 上次保留的CCR值
unsigned short Last_CCR = 0;
// 累积定时器溢出数
unsigned short Counter = 0;
// 记录的周期值
unsigned long RecTime = 0;
// 捕获完成标志位，指示主程序已经完成一次捕获
unsigned char CapFinshed = 0;

// 显存 - 4Byte
unsigned long DispBuf = 0;

void main(void)
{
	WDTCTL = WDTPW | WDTHOLD;		// stop watchdog timer

	// P2.1 作为信号输入
	P2SEL &= ~0x02;      // 根据数据手册查询P2SEL.1 = 0, P2SEL2.1=1, P2DIR.1=0为Timer1_A3.CCI1A输入
	P2SEL2 |= 0x02;     //
	P2DIR &= 0x02;      //

	P1SEL = 0x00;
	P1SEL2 = 0x00;
	P1DIR = 0xFF;
	P1OUT = 0xFF;

	P3SEL = 0x00;
	P3SEL2 = 0x00;
	P3DIR = 0xFF;
	P3OUT = 0x00;

	TA1CCTL1 = CM_1 | CCIS_0 | CAP | CCIE;
	//      上升沿捕获 | CCISx_A | 捕获模式 | 捕获中断
	TA1CTL = TASSEL_2 | ID_0 | TAIE | MC_2 | TACLR;
	//      1MHz | Div 1 | 溢出中断 | 连续计数模式 | Clear TAR

	TACCTL0 = CCIE;
	TACCR0 = 249;   // 2ms
	TACTL = TASSEL_2 | ID_3 | MC_1 | TACLR; // 125KHz UpMode

	__enable_interrupt();  // 打开全局中断

	while(1)
	{
	    if(CapFinshed)  // 有一次捕获结果了
	    {
	        DispBuf = (RecTime );    //显示微秒为单位的周期值，当前为16进制显示，可以自行在此转换为10进制
	        CapFinshed = 0; //处理结束，清除标志位
	    }
	}
}

#pragma vector = TIMER1_A1_VECTOR
__interrupt void Timer1_Isr(void)
{
    switch (TA1IV)
    {
    case 0x02:  // CCR1 中断
        RecTime = ((unsigned long)Counter << 16) + TA1CCR1 - Last_CCR; // Counter * 65536
        Last_CCR = TA1CCR1;
        Counter = 0;
        TA1CCTL1 &= ~CCIFG;
        CapFinshed = 1;
        break;
    case 0x04:  // CCR2 中断
        TA1CCTL2 &= ~CCIFG;
        break;
    case 0x0A:  // 溢出中断
        Counter ++;
        TA1CTL &= ~TAIFG;
        break;
    }
}

// 共阳数码管字库，连接顺序（正序）：LEDDP - P27 ... LEDA - P20
//unsigned char DispLib[16] = {0xC0, 0xF9, 0xA4, 0xB0, 0x99, 0x92, 0x82, 0xF8, 0x80, 0x90, 0x88, 0x83, 0xC6, 0xA1, 0x86, 0x8E};
// 共阴数码管字库，连接顺序（正序）：LEDDP - P27 ... LEDA - P20
const unsigned char DispLib[16] = {0x3F, 0x06, 0x5B, 0x4F, 0x66, 0x6D, 0x7D, 0x07, 0x7F, 0x6F, 0x77, 0x7C, 0x39, 0x5E, 0x79, 0x71};

#pragma vector = TIMER0_A0_VECTOR
__interrupt void Timer0_Isr(void)
{
    static unsigned char cnt = 0;
    unsigned char tmp;
    P1OUT = 0xFF;   // 所有数码管先熄灭，以免后面段码变化造成虚影现象
    // tmp 是取出当前要显示数字的那4个bit
    // (cnt << 2) 是每个数字占4bit，即(cnt * 4)，为了节省计算复杂度，采用(cnt<<2)
    tmp = (unsigned char)(DispBuf >> (cnt << 2));
    P3OUT = DispLib[tmp & 0x0F];    // 必须要屏蔽高4bit，所以采用tmp & 0x0F操作
    // 将对应要显示的数码管位置0，其余置1
    // (0x01 << cnt)是将对应位置一，其余为0
    P1OUT = ~(0x01 << cnt);

    // 准备下一位
    cnt ++;
    if(cnt > 7)
        cnt = 0;

}

