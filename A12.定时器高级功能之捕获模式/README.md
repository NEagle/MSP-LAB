# 定时器高级功能 - 捕获模式

## 课程讲解内容

## 实验内容：测量时基生成电路的波形周期值

### 实验目的：
#### 1、定时器捕获功能的验证
#### 2、测量周期值的完整程序，并将测量结果显示在扫描数码管

### 实验例程：
#### 1、MSP-LAB系统连线准备
    a、P10~P17 与 扫描数码管的 位使能DIG1~DIG8相连
    b、P30~P37 与 扫描数码管的 段使能DLEDA~DLEDDP相连
    c、P21与 时基生成电路的NEOUT相连
    d、跳帽开关确保DIGVCC、NEVCC与3V3短接，保证相应模块供电正常

#### 2、例程运行
    a、使用 capture.c 构建工程并编译下载；
    b、全速运行状态：
        i/ 初始状态，数码管显示八个‘0’
        ii/ 马上进入测量状态，显示时基生成电路发生的波形的周期值，数值以16进制显示，单位位us
        iii/ 调整时基生成发生电路的可调电阻，会改变周期值

#### 3、用户改变
    a、同学们可以改变显示方式（如十进制）
    b、改变捕获通道、捕获代码，查看效果

## 增加了“display_example”的数码管封装模板程序

### 例程目的：
#### 1、提供多文件工程模板
#### 2、提供将数码管按照模块封装的工程模板，有利于学习积累

### 硬件设计/连线要求：
#### 1、P1.0~P1.7连接八位数码管的位码，P1.7接高位
#### 2、P3.0~P3.7连接八位数码管的显示内容控制的段码，P1.7连接DP。。。P1.0连接A
#### 3、采用八位共阴数码管
