/*
 * display.h
 * 本文将设计标准化数码管显示接口问题就
 * 以共阴八位数码管为设计目标
 * 段码设计按照Px.7-Dp 。。。 Px.0-A的连接方式
 *  Created on: 2023年5月4日
 *      Author: Jun Ying
 */

#ifndef DISPLAY_H_
#define DISPLAY_H_

//使用管脚方式定义如下，其他不同连接方式，请自行修改
//段码定义，连接方式：P3.7 - DP 。。。 P3.0 - A
//位码定义，连接方式：P1.7 - 高位 。。。 P1.0 - 低位
//采用八位共阴数码管
//使用的定时器 Timer0

//八个BSD显示的数码管数据类型定义
typedef struct {
    unsigned int BSD0:4;    //低4bit - 对应最低数码管显示内容
    unsigned int BSD1:4;
    unsigned int BSD2:4;
    unsigned int BSD3:4;
    unsigned int BSD4:4;
    unsigned int BSD5:4;
    unsigned int BSD6:4;
    unsigned int BSD7:4;    //高4bit - 对应最高数码管显示内容
} DispBit4;

//数码管显示缓冲器类型定义
typedef union {
    unsigned long DispLong; //按照32bit组织的显存
    unsigned char DispChar[4];  //按照byte组织的显存
    DispBit4 DispValue;     //按照BSD组织的显存
} CubeDisp_U;
//显示帧存，对应八位数码管，以十六进制存储
extern CubeDisp_U DispBuf;
//DP显示控制，8bit对应八个数码管的DP显示与否
extern unsigned char DispDp;
//中划线显示控制，8bit对应八个数码管的中划线功能
extern unsigned char DispDash;
//消隐控制，8bit对应八个数码管的消隐控制与否
extern unsigned char DispDis;

//数码管初始化函数
//要求P3.0~P3.7连接段码，P1.0~P1.7连接位码，八位共阴数码管
void InitDisplay(void);

#endif /* DISPLAY_H_ */
