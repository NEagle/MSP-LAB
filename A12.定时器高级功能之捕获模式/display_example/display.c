/*
 * display.c
 *
 *  Created on: 2023年5月4日
 *      Author: 401
 */

#include <msp430.h>
#include "display.h"

//显示帧存，对应八位数码管，以十六进制存储
CubeDisp_U DispBuf;//DP显示控制

unsigned char DispDp = 0;
//中划线显示控制
unsigned char DispDash = 0;
//消隐控制
unsigned char DispDis = 0;

void InitDisplay(void)
{
    //位码管脚定义
    P1SEL = 0x00;
    P1SEL2 = 0x00;
    P1DIR = 0xFF;
    P1OUT = 0xFF;
    //段码管脚定义
    P3SEL = 0x00;
    P3SEL2 = 0x00;
    P3DIR = 0xFF;
    P3OUT = 0x00;

    TACCTL0 = CCIE;
    TACCR0 = 249;   // 2ms
    TACTL = TASSEL_2 | ID_3 | MC_1 | TACLR; // 125KHz UpMode

    DispBuf.DispLong = 0x12345678;
    __enable_interrupt();  // 打开全局中断
}

// 共阳数码管字库，连接顺序（正序）：LEDDP - P27 ... LEDA - P20
//unsigned char DispLib[16] = {0xC0, 0xF9, 0xA4, 0xB0, 0x99, 0x92, 0x82, 0xF8, 0x80, 0x90, 0x88, 0x83, 0xC6, 0xA1, 0x86, 0x8E};
// 共阴数码管字库，连接顺序（正序）：LEDDP - P27 ... LEDA - P20
const unsigned char DispLib[16] = {0x3F, 0x06, 0x5B, 0x4F, 0x66, 0x6D, 0x7D, 0x07, 0x7F, 0x6F, 0x77, 0x7C, 0x39, 0x5E, 0x79, 0x71};
#define NoDisp 0x00     //当前位不显示
#define DashDisp 0x40   //当前位显示中划线
#define DotDisp 0x80    //当前位的小数点

#pragma vector = TIMER0_A0_VECTOR
__interrupt void Timer0_Isr(void)
{
    static unsigned char cnt = 0;
    unsigned char buf, bit;
    P1OUT = 0xFF;   // 所有数码管先熄灭，以免后面段码变化造成虚影现象
    // bit作为当前轮询到的位
    bit = 0x01 << cnt;
    if(DispDis & bit)
        P3OUT = NoDisp;   //消隐生效
    else if(DispDash & bit)
        P3OUT = DashDisp;
    else {
        // tmp作为当前计算的
        // tmp 是取出当前要显示数字的那4个bit
        // (cnt << 2) 是每个数字占4bit，即(cnt * 4)，为了节省计算复杂度，采用(cnt<<2)
        buf = (unsigned char)(DispBuf.DispLong >> (cnt << 2));
        if(DispDp & bit)
            buf |= DotDisp;
        P3OUT = DispLib[buf & 0x0F];    // 必须要屏蔽高4bit，所以采用tmp & 0x0F操作
    }
    // 将对应要显示的数码管位置0，其余置1
    // (0x01 << cnt)是将对应位置一，其余为0
    P1OUT = ~bit;

    // 准备下一位
    cnt ++;
    if(cnt > 7)
        cnt = 0;

}
