#include <msp430.h>				
#include "display.h"

/**
 * blink.c
 */
void Delay_Ms(int time)
{
    unsigned char i;
    while(time --)
        for(i=0; i<100; i++)
            ;
}

void main(void)
{
	WDTCTL = WDTPW | WDTHOLD;		// stop watchdog timer
	InitDisplay();

	DispBuf.DispValue.BSD0 = 9;     //测试直接赋值某个数码管
    DispBuf.DispValue.BSD1 = 0xB;   //测试直接赋值某个数码管
    DispBuf.DispValue.BSD7 = 0xA;   //测试直接赋值某个数码管

	while(1)
	{
	    DispBuf.DispLong ++;
	    Delay_Ms(1000);
	}
}
