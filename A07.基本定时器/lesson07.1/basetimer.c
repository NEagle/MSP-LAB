#include <msp430.h>				


/**
 * 基本定时器例程
 */

// 硬件连接
// P3.3 - LED

void main(void)
{
	WDTCTL = WDTPW | WDTHOLD;		// stop watchdog timer

	P3SEL &= ~0x08;     //P3.3 as GPIO
	P3SEL2 &= ~0x08;    //P3.3 as GPIO
	P3DIR |= 0x08;      //P3.3 as Output
	P3OUT &= ~0x08;     //LED be off

	TA0CCTL0 = CCIE;    //CCR0 interrupt ON
	TACCR0 = 12499;     // 100ms -> 1M/8/(12499+1) = 125000/12500 = 10 clock per 1s
	TACTL = TASSEL_2 | ID_3 | MC_1 | TACLR;
	        // SMCLK | Div 8| UpMode| Clear TAR

	__enable_interrupt();  // 打开全局中断

	while(1)
	{
	    ;
	}
}

#pragma vector = TIMER0_A0_VECTOR
__interrupt void Timer0_Isr(void)
{
    P3OUT ^= 0x08;  // P3.3 Toggle
}
