# 基本定时器

## 课程讲解内容

## 实验内容：使用定时器控制LED闪烁

### 实验目的：
#### 1、定时器设置方法，含时间/周期计算方法
#### 2、定时器中断设置及中断服务程序撰写

### 实验例程：
#### 1、MSP-LAB系统连线准备
    a、J26的P33 与J29的PWM 相连；

#### 2、例程编译运行
    a、使用basetimer.c构建工程并编译下载；
    b、全速运行时，则LED以5Hz的精确时间闪烁；
