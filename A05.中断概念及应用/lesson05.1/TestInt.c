#include <msp430.h>				


/**
 * GPIO Test
 */

void Delay_ms(int ms)
{
    int n;
    while(ms--)
    {
        // 每次循环大约10个clock，1MHz情况下，循环100次，为1000个clock，大约1ms
        // 注意需要确认工程设置中优化选项要关闭，否则程序会被忽略
        for(n = 100; n--; n>0)
            ;
    }
}

// 硬件连接
// P1.4 - LED
// P2.1 - KEY

// 全局变量，保存LED的状态， 0 - 熄灭，1 - 常亮， 2 - 闪烁
// 全局变量作为中断服务程序与主程序之间信息传递用
unsigned char LED_STA = 0;

void main(void)
{
	WDTCTL = WDTPW | WDTHOLD;		// stop watchdog timer

	P1SEL &= ~0x10;     // 设置P10为GPIO
	P1SEL2 &= ~0x10;    // 设置P10为GPIO
	P1DIR |= 0x10;		// 设置P10为输入模式
	P1OUT |= 0x10;     // P1.4初始化时为高电平

	P2SEL &= ~0x02;     // 设置P21为GPIO
	P2SEL2 &= ~0x02;    // 设置P21为GPIO
	P2DIR &= 0x02;      // 设置P21为输入
	P2REN |= 0x02;      // 打开P21的上下拉使能
	P2OUT |= 0x02;      // 设置P21为上拉
	P2IE |= 0x02;       // 打开P21的中断使能
	P2IES |= 0x02;      // 下降沿为中断触发
	P2IFG &= ~0x02;     // 清除中断标志位，为下一次中断准备

	__enable_interrupt();  // 打开全局中断

	while(1)
	{
	    switch(LED_STA) // 根据状态字来进行不同的动作
	    {
	    case 0 :    // 熄灭态
	        P1OUT &= ~0x10;
	        break;
	    case 1 :    // 常亮态
	        P1OUT |= 0x10;
	        break;
	    case 2 :    // 闪烁态
	        P1OUT ^= 0x10;  // LED翻转
	        Delay_ms(500);
	        break;
	    }
	}
}

#pragma vector = PORT2_VECTOR
__interrupt void Port2_Isr(void)
{
    if(P2IFG & 0x02)    // 检查P21中断标志位
    {
        Delay_ms(10);   // 前沿去抖
        if(!(P2IN & 0x02))  // 再次判断是否为可靠的按键
        {
            while(!(P2IN & 0x02))   // 等待按键抬起
                ;
            Delay_ms(10);   // 后沿去抖
            LED_STA ++; // 切换状态
            if(LED_STA > 2) // 当超过所有状态值，回到初始状态
                LED_STA = 0;
        }
        P2IFG &= ~0x02; // 因为P2中断为共享中断，所以需要手动清除标志位，为下次中断准备
    }
}
