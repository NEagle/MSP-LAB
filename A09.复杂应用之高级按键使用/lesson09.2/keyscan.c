#include <msp430.h>				


/**
 * 矩阵键盘识别
 * 采用扫描识别方法
 */

// 延迟函数，毫秒为单位
void Delay_ms(int ms)
{
    int n;
    while(ms--)
    {
        // 每次循环大约10个clock，1MHz情况下，循环100次，为1000个clock，大约1ms
        // 注意需要确认工程设置中优化选项要关闭，否则程序会被忽略
        for(n = 100; n--; n>0)
            ;
    }
}

// 延迟函数，延迟1us
void Delay_1us(void)
{
    return ;
}

// 硬件连接
// P3 - 扫描数码管的段码
// P2 - 扫描数码管八个位码
// P1.1 - 按键

// 八位数码管显示的内容缓存
unsigned long DispBuf = 0;

// 矩阵键盘扫描码表
unsigned char KeyCodeTable[16] = {  0xEE, 0xDE, 0xBE, 0x7E, 0xED, 0xDD, 0xBD, 0x7D,
                                    0xEB, 0xDB, 0xBB, 0x7B, 0xE7, 0xD7, 0xB7, 0x77
};

// 循环左移函数，输出是输入val左移n位，最高位移入最低位
unsigned char crol(unsigned char val, unsigned char n)
{
    unsigned char bit0;
    if(val & 0x80)
        bit0 = 1;
    else
        bit0 = 0;
    for(;n>0; n--)
        val = (val << 1) | bit0;
    return val;
}

// 按键扫描逻辑
// 返回值为按键编码值，0~15为有效值，对应4x4按键中的某一个，16为无效值
unsigned char Keys_Scan(void)
{
    unsigned char t=0xFE;   // 缺省最低位输出0，其余均为1
    unsigned char i,j, temp;
    P1OUT = 0xF0;   // 4位列输出均为0，检查有无按键
    Delay_1us();
    if((P1IN & 0xF0) != 0xF0)   // 当有按键，则高四位必有某位为0
    {
        for(i=0; i<4; i++)  // 扫描输出循环
        {
            P1OUT = t;  // 低四位依次输出0
            temp = P1IN;
            for(j=0; j<16; j++) //匹配码表
                if(temp == KeyCodeTable[j]) //匹配到，则直接返回
                    return(j);
            t=crol(t,1);    //循环左移，下一个bit置0
        }
    }
    return(16); //无有效按键，返回16
}

void main(void)
{
    unsigned char Key_Value = 16;
	WDTCTL = WDTPW | WDTHOLD;		// stop watchdog timer

	// P1.1作为按键输入，但不开启中断
	P1SEL = 0x00;   //P1 as GPIO
	P1SEL2 = 0x00;  //P1 as GPIO
	P1DIR = 0x0F;   //P1.0~P1.3 as Output, P1.4~P1.7 as Input
	P1REN |= 0xF0;  // 上下拉电阻使能
	P1OUT |= 0xF0;  // 上拉电阻

	P2SEL = 0x00;   // P2 all as GPIO
	P2SEL2 = 0x00;  // P2 all as GPIO
	P2DIR = 0xFF;   // P2 all as Output
	P2OUT = 0xFF;   // All cube disable

	P3SEL = 0x00;   // P3 all as GPIO
	P3SEL2 = 0x00;  // P3 all as GPIO
	P3DIR = 0x0FF;  // P3 all as Output
	P3OUT = 0x00;   // All dot disable

	//TA0 作为扫描数码管刷新的时间计数
	TACCTL0 = CCIE;  // Timer 0 CCR0 Interrupt Enable
	TACCR0 = 249; // 1M/8/(499+1) = 125000/250 = 500 (2ms)
	// 尝试修改定时器时间，看看显示效果，以及思考如何计算
	TACTL = TASSEL_2 | ID_3 | MC_1 | TACLR;
	//      1MHz | Div 8 | Up Mode | Clear TAR

	__enable_interrupt();  // 打开全局中断

	while(1)
	{
	    P1OUT = 0xF0;   // 先检测有无按键
	    Delay_1us();
	    if((P1IN & 0xF0) != 0xF0)   // 有按键时
	    {
	        Delay_ms(10);   // 前沿去抖
	        if((P1IN & 0xF0) != 0xF0)   // 再次确认是否有按键
	        {
	            Key_Value = Keys_Scan();    // 扫描矩阵键盘
	            P1OUT = 0xF0;
	            Delay_1us();
	            while((P1IN & 0xF0) != 0xF0)    // 扫描结束，仍然按着，则一直停留，等待抬起
	            {
	                P1OUT = 0xF0;
	            }
	        }
	    }
	    if(Key_Value == 16) //没有按键
	        DispBuf = 0x12345678;
	    else
	        DispBuf = Key_Value;    // 输出按键值
	}
}

// 共阳数码管字库，连接顺序（正序）：LEDDP - P27 ... LEDA - P20
//unsigned char DispLib[16] = {0xC0, 0xF9, 0xA4, 0xB0, 0x99, 0x92, 0x82, 0xF8, 0x80, 0x90, 0x88, 0x83, 0xC6, 0xA1, 0x86, 0x8E};
// 共阴数码管字库，连接顺序（正序）：LEDDP - P27 ... LEDA - P20
const unsigned char DispLib[16] = {0x3F, 0x06, 0x5B, 0x4F, 0x66, 0x6D, 0x7D, 0x07, 0x7F, 0x6F, 0x77, 0x7C, 0x39, 0x5E, 0x79, 0x71};

#pragma vector = TIMER0_A0_VECTOR
__interrupt void Timer0_Isr(void)
{
    static unsigned char cnt = 0;
    unsigned char tmp;
    P2OUT = 0xFF;   // 所有数码管先熄灭，以免后面段码变化造成虚影现象
    // tmp 是取出当前要显示数字的那4个bit
    // (cnt << 2) 是每个数字占4bit，即(cnt * 4)，为了节省计算复杂度，采用(cnt<<2)
    tmp = (unsigned char)(DispBuf >> (cnt << 2));
    P3OUT = DispLib[tmp & 0x0F];    // 必须要屏蔽高4bit，所以采用tmp & 0x0F操作
    // 将对应要显示的数码管位置0，其余置1
    // (0x01 << cnt)是将对应位置一，其余为0
    P2OUT = ~(0x01 << cnt);

    // 准备下一位
    cnt ++;
    if(cnt > 7)
        cnt = 0;
}

