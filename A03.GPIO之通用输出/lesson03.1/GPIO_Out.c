#include <msp430.h>				


/**
 * GPIO 输出功能测试
 */

void Delay_ms(int ms)
{
    int n;
    while(ms--)
    {
        // 每次循环大约10个clock，1MHz情况下，循环100次，为1000个clock，大约1ms
        // 注意需要确认工程设置中优化选项要关闭，否则程序会被忽略
        for(n = 100; n--; n>0)
            ;
    }
}

void main(void)
{
	WDTCTL = WDTPW | WDTHOLD;		// stop watchdog timer

	P1SEL = 0x00;       // 设置P10~P17均为GPIO
	P1SEL2 = 0x00;      // 设置P10~P17均为GPIO
	P1DIR = 0xFF;		// 设置P10~P17均为输出模式

	unsigned char Buf = 0x01;   // 中间变量

	while(1)
	{
		P1OUT = Buf;    // 修改输出内容
		Delay_ms(1000); // 等待1s
		Buf = Buf<< 1;  // 修改中间变量
		if(Buf == 0)    // 若移位溢出，则重新设置初始值
		    Buf = 0x01;
	}
}
