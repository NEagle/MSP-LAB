/*
 * ======== Standard MSP430 includes ========
 */
#include <msp430.h>

/*
 * ======== Grace related declaration ========
 */
extern void Grace_init(void);

/*
 *  ======== main ========
 */
int main( void )
{
    extern char Char2Disp;
    Grace_init();                     // Activate Grace-generated configuration
    
    // >>>>> Fill-in user code here <<<<<
    while(1)
    {
        for(Char2Disp = '0'; Char2Disp <= '9'; Char2Disp++)
        {
            __delay_cycles(1000000);
        }
        for(Char2Disp = 'A'; Char2Disp <= 'Z'; Char2Disp++)
        {
            __delay_cycles(1000000);
        }
        for(Char2Disp = 'a'; Char2Disp <= 'z'; Char2Disp++)
        {
            __delay_cycles(1000000);
        }
    }
//    return (0);
}
