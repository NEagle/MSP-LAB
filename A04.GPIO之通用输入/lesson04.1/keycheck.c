#include <msp430.h>				


/**
 * GPIO 输入检测
 */

void Delay_ms(int ms)
{
    int n;
    while(ms--)
    {
        // 每次循环大约10个clock，1MHz情况下，循环100次，为1000个clock，大约1ms
        // 注意需要确认工程设置中优化选项要关闭，否则程序会被忽略
        for(n = 100; n--; n>0)
            ;
    }
}

void main(void)
{
	WDTCTL = WDTPW | WDTHOLD;		// stop watchdog timer

	P1SEL &= ~0x01;     // 设置P10为GPIO
	P1SEL2 &= ~0x01;    // 设置P10为GPIO
	P1DIR &= ~0x01;		// 设置P10为输入模式
	P1REN |= 0x01;      // 打开上下拉电阻使能
	P1OUT |= 0x01;      // 设置上拉电阻

	P2SEL &= ~0x01;     // 设置P20为GPIO
	P2SEL2 &= ~0x01;    // 设置P20为GPIO
	P2DIR |= 0x01;      // 设置P20为输出
	P2OUT &= ~0x01;     // P20输出低电平

	while(1)
	{
	    if((P1IN & 0x01) == 0x00)   // 检查P10是否为低
	    {
	        Delay_ms(10);   // 前沿去抖
	        if(!(P1IN & 0x01))  // 再次检查P10的电平
	        {
	            while (!(P1IN & 0x01))  // 等待按键抬起
	                ;
	            Delay_ms(10);   // 后沿去抖
	            P2OUT ^= 0x01;  // LED状态切换
	        }
	    }
	}
}
