#include <msp430.h>				


/**
 * 数码管显示例程
 */

void Delay_ms(int ms)
{
    int n;
    while(ms--)
    {
        // 每次循环大约10个clock，1MHz情况下，循环100次，为1000个clock，大约1ms
        // 注意需要确认工程设置中优化选项要关闭，否则程序会被忽略
        for(n = 100; n--; n>0)
            ;
    }
}

// 硬件连接
// P27~P20 - LEDDP~LEDA
// P1.2 - KEY

// 共阳数码管字库，连接顺序（正序）：LEDDP - P27 ... LEDA - P20
unsigned char DispLib[16] = {0xC0, 0xF9, 0xA4, 0xB0, 0x99, 0x92, 0x82, 0xF8, 0x80, 0x90, 0x88, 0x83, 0xC6, 0xA1, 0x86, 0x8E};
// 共阴数码管字库，连接顺序（正序）：LEDDP - P27 ... LEDA - P20
//unsigned char DispLib[16] = {0x3F, 0x06, 0x5B, 0x4F, 0x66, 0x6D, 0x7D, 0x07, 0x7F, 0x6F, 0x77, 0x7C, 0x39, 0x5E, 0x79, 0x71};


void main(void)
{
	WDTCTL = WDTPW | WDTHOLD;		// stop watchdog timer

	P1SEL &= ~0x04;     // 设置P12为GPIO
	P1SEL2 &= ~0x04;    // 设置P12为GPIO
	P1DIR &= ~0x04;		// 设置P12为输入模式
	P1REN |= 0x04;  // 设置上下拉电阻使能
	P1OUT |= 0x04;  // 设置上拉电阻
	P1IE |= 0x04;   // 设置中断使能
	P1IES |= 0x04;  // 下拉触发
	P1IFG &= ~0x04; // 清除IFG

	P2SEL = 0x00;   // 设置P2为GPIO
	P2SEL2 = 0x00;  // 设置P2为GPIO
	P2DIR = 0xFF;   // 设置P2为输出
	P2OUT = 0xFF;   // 初始化后全熄灭（共阳数码管）

	__enable_interrupt();  // 打开全局中断

	while(1)
	{
	    ;	// 没有业务逻辑
	}
}

#pragma vector = PORT1_VECTOR
__interrupt void Port1_Isr(void)
{
    // 静态变量，退出本函数后，该变量保持不变
    static unsigned char Dig = 0;   // 仅初始化（第一次）有效

    if(P1IFG & 0x04)    // 检查P12中断标志位
    {
        Delay_ms(10);   // 前沿去抖
        if(!(P1IN & 0x04))  // 再次判断是否为可靠的按键
        {
            while(!(P1IN & 0x04))   // 等待按键抬起
                ;
            Delay_ms(10);   // 后沿去抖

            // 业务逻辑，执行输出之后，Dig自增一
            // 与 0x0F，即取最后四bit，限制数组的下标取值范围为0~15
            P2OUT = DispLib[(Dig ++) & 0x0F];
        }
        P1IFG &= ~0x04; // 因为P2中断为共享中断，所以需要手动清除标志位，为下次中断准备
    }
}
