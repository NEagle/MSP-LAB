#include <msp430.h>				


/**
 * PWM波形发生
 */

// 硬件连接
// P1.2 - 连接 标志PWM的LED

void main(void)
{
	WDTCTL = WDTPW | WDTHOLD;		// stop watchdog timer

	// P1.2 作为PWM输出
	P1SEL |= 0x04;      // 根据数据手册查询P1SEL.2 = 1, P1SEL2.2=0, P1DIR.2=1为TA0.1输出
	P1SEL2 &= ~0x04;    //P11 as GPIO
	P1DIR |= 0x04;      //P11 as Input

	TACCTL0 = CCIE;  // Timer 0 CCR0 Interrupt Enable
	TACCR0 = 999; // 1ms
	TACCTL1 = OUTMOD_7; // 比较模式7（清零/置位）
	TACCR1 = 1;    // 起始占空比为1/20
	TACTL = TASSEL_2 | ID_3 | MC_1 | TACLR;
	//      1MHz | Div 8 | Up Mode | Clear TAR

	__enable_interrupt();  // 打开全局中断

	while(1)
	{
	}
}

#pragma vector = TIMER0_A0_VECTOR
__interrupt void Timer0_Isr(void)
{
    // 静态变量，作为多次中断的次数统计用
    static unsigned short Counter = 0;
    Counter ++;
    if((Counter & 0x1FF) == 0)  // 每512次执行一次，即0.512S执行一次变化
    {
        TACCR1 += 50;   // 调整PWM的占空比
        if(TACCR1 > 999)    //超出100%
            TACCR1 = 1;    //从头开始
    }
}
