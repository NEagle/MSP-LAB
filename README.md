# MSP-LAB

#### 介绍
MSP-LAB是基于TI MSP430的单片机学习系统，本仓库拟定在基于MSP-LAB系统进行单片机从入门到精通地开展学习。拟定涵盖《单片机原理及应用》课程的学习内容，以及基于单片机的技巧例程、单片机常见程序框架等。

### MSP-LAB的实物图
![MSP-LAB.png](./img/MSP-LAB.png "MSP-LAB实物图")

#### 软件架构
MSP-LAB系统的主处理器为TI公司的MSP430G2553(http://www.ti.com/product/MSP430G2553)。所有程序会在TI CCS软件（http://www.ti.com/tool/CCSTUDIO）上进行验证。用户需要自行下载安装CCS软件。


#### 安装教程

1. CCS下载：http://software-dl.ti.com/ccs/esd/documents/ccs_downloads.html
2. CCS安装：https://software-dl.ti.com/ccs/esd/documents/users_guide/ccs_installation.html
3. 本仓库例程导入到CCS工作目录后，用CCS编译、下载到MSP-LAB硬件系统、调试运行。

#### 使用说明

1. 所有例程将被基于MSP430G2553的MSP-LAB硬件系统测试。
2. 所有例程需要一定的连线基础上测试成功，因此需要用户仔细阅读每个例程的连线说明，并确保正确连线之后，上电测试。
3. 所有例程将使用C语言进行编程。
4. 原理图参考 ![MSP-LAB-SCH.jpg](./img/MSP-LAB-SCH.jpg "原理图")

#### 参与贡献



