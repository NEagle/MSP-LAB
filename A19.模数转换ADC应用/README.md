# ADC简单应用

## 课程讲解内容

## 实验内容：使用ADC采样滑动变阻器的中心抽头电压

### 实验目的：
#### 1、GRACE配置ADC相关外设并学习各配置项的意义；
#### 2、掌握ADC结果计算的方法；

### 实验例程：
#### 1、MSP-LAB系统连线准备
    a、P1.0与滑动变阻器的中心抽头相连，滑动变阻器的供电跳帽与3.3V相连；
    b、P2.0~P2.7与扫描数码管的DLEDa~DLEDdp相连；
    c、P3.0~P3.7与扫描数码管的DIG1~DIG8相连；
    d、确保DIGVCC与3.3V跳帽开关连通；

#### 2、GRACE生成代码修改：
    a、main主函数，添加while(1)循环；
    b、中断向量文件InterruptVectors_init.c文件中添加下面业务逻辑：
        i/定时器中断，添加扫描数码管的扫描逻辑
        ii/ADC中断中添加ADC数据读取、16进制转10进制逻辑，并最后填入帧存DispBuf。

#### 3、例程运行
    a、创建工程后，将修改后的GRACE生成代码拷入工程目录，并添加GRACE安装目录的include搜索路径；
    b、编译下载，全速运行，则数码管显示ADC采样值和转换后的电压值，调整滑动变阻器的旋钮，观看数值变化。
