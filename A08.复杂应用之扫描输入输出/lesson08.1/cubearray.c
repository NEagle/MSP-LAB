#include <msp430.h>				


/**
 * 扫描数码管显示例程
 */

void Delay_ms(int ms)
{
    int n;
    while(ms--)
    {
        // 每次循环大约10个clock，1MHz情况下，循环100次，为1000个clock，大约1ms
        // 注意需要确认工程设置中优化选项要关闭，否则程序会被忽略
        for(n = 100; n--; n>0)
            ;
    }
}

// 硬件连接
// P3 - 扫描数码管的段码
// P2 - 扫描数码管八个位码
// P1.1 - 按键

void main(void)
{
	WDTCTL = WDTPW | WDTHOLD;		// stop watchdog timer

	P1SEL &= ~0x02; //P1.1 as GPIO
	P1SEL2 &= ~0x02;    //P1.1 as GPIO
	P1DIR &= ~0x02; //P1.1 as Input
	P1REN |= 0x02;  // 上下拉电阻使能
	P1OUT |= 0x02;  // 上拉电阻
	P1IE |= 0x02;   // Enable the interrupt
	P1IES |= 0x02;  // Down Edge trig

	P2SEL = 0x00;   // P2 all as GPIO
	P2SEL2 = 0x00;  // P2 all as GPIO
	P2DIR = 0xFF;   // P2 all as Output
	P2OUT = 0xFF;   // All cube disable

	P3SEL = 0x00;   // P3 all as GPIO
	P3SEL2 = 0x00;  // P3 all as GPIO
	P3DIR = 0x0FF;  // P3 all as Output
	P3OUT = 0x00;   // All dot disable

	TACCTL0 = CCIE;  // Timer 0 CCR0 Interrupt Enable
	TACCR0 = 249; // 1M/8/(499+1) = 125000/250 = 500 (2ms)
	// 尝试修改定时器时间，看看显示效果，以及思考如何计算
	TACTL = TASSEL_2 | ID_3 | MC_1 | TACLR;
	//      1MHz | Div 8 | Up Mode | Clear TAR

	__enable_interrupt();  // 打开全局中断

	while(1)
	{
	    ;
	}
}

// 共阳数码管字库，连接顺序（正序）：LEDDP - P27 ... LEDA - P20
//unsigned char DispLib[16] = {0xC0, 0xF9, 0xA4, 0xB0, 0x99, 0x92, 0x82, 0xF8, 0x80, 0x90, 0x88, 0x83, 0xC6, 0xA1, 0x86, 0x8E};
// 共阴数码管字库，连接顺序（正序）：LEDDP - P27 ... LEDA - P20
const unsigned char DispLib[16] = {0x3F, 0x06, 0x5B, 0x4F, 0x66, 0x6D, 0x7D, 0x07, 0x7F, 0x6F, 0x77, 0x7C, 0x39, 0x5E, 0x79, 0x71};

// 八位数码管显示的内容缓存
unsigned long DispBuf = 0x12345678;

#pragma vector = TIMER0_A0_VECTOR
__interrupt void Timer0_Isr(void)
{
    static unsigned char cnt = 0;
    unsigned char tmp;
    P2OUT = 0xFF;   // 所有数码管先熄灭，以免后面段码变化造成虚影现象
    // tmp 是取出当前要显示数字的那4个bit
    // (cnt << 2) 是每个数字占4bit，即(cnt * 4)，为了节省计算复杂度，采用(cnt<<2)
    tmp = (unsigned char)(DispBuf >> (cnt << 2));
    P3OUT = DispLib[tmp & 0x0F];    // 必须要屏蔽高4bit，所以采用tmp & 0x0F操作
    // 将对应要显示的数码管位置0，其余置1
    // (0x01 << cnt)是将对应位置一，其余为0
    P2OUT = ~(0x01 << cnt);

    // 准备下一位
    cnt ++;
    if(cnt > 7)
        cnt = 0;
}

#pragma vector = PORT1_VECTOR
__interrupt void Port1_ISR(void)
{
    if(P1IFG & 0x02)
    {
        Delay_ms(10);
        if(!(P1IN & 0x02))
        {
            while(!(P1IN & 0x02))
                ;
            Delay_ms(10);
            // 修改显示内容的具体数据
            DispBuf ++;
        }
        P1IFG &= ~0x02;
    }
}
