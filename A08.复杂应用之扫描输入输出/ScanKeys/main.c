// 本程序为扫描按键的示例程序
// 硬件连接方式：
//  P1.0~P1.3连接矩阵键盘的行0~行3
//  P1.4~P1.7连接矩阵键盘的列0~列3
//  P3.0~P3.7连接数码管的A~DP（使用共阴扫描数码管，找出某一个数码管位接地）
// 实现效果
//  上电运行，数码管不显示
//  当矩阵键盘按过某个按键，在数码管上显示0~F的值，其中每个数值对应某个矩阵键盘中的按键编号

#include <msp430.h> 

void display(unsigned char a);
void Delay10ms();
unsigned char scan();

//共阴数码管的字库
const unsigned char DispLib[16]={0x3F, 0x06, 0x5B, 0x4F, 0x66, 0x6D, 0x7D, 0x07, 0x7F, 0x6F, 0x77, 0x7C, 0x39, 0x5E, 0x79, 0x71};

unsigned char key = 16;
/**
 * main.c
 */
int main(void)
{
    WDTCTL = WDTPW | WDTHOLD;   // stop watchdog timer

    //按键管脚初始化
    P1SEL = 0x00;   //P1.0~P1.7 设置为GPIO
    P1SEL2 = 0x00;
    P1DIR = 0x0F;   //P1.0~P1.3 设置为输出，P1.4~P1.7设置为输入
    P1OUT = 0xFF;   //P1.0~P1.3 设置输出高，P1.4~P1.7设置上拉电阻
    P1REN = 0xF0;   //P1.4~P1.7 设置上下拉电阻使能

    //数码管管脚初始化
    P3SEL = 0x00;   //P3.0~P3.7 设置为GPIO
    P3SEL2 = 0x00;
    P3DIR = 0xFF;   //P3.0~P3.7 设置为输出
    P3OUT = 0x00;   //输出全低，数码管不点亮
    while(1)
    {
        display(scan());
    }

}


void display(unsigned char a)
{
    if (a<16)
        P3OUT=DispLib[a&0x0F];
    Delay10ms();
}

#define RowDis  0x0F
#define Row0    0xFE
#define Row1    0xFD
#define Row2    0xFB
#define Row3    0xF7
#define Col0    0x10
#define Col1    0x20
#define Col2    0x40
#define Col3    0x80
unsigned char scan()
{
    //第0行扫描
    P1OUT |= RowDis;   //所有管脚均置高
    P1OUT &= Row0;
    if((P1IN & Col0)==0)    //检测第0列
    {
        Delay10ms();
        if((P1IN & Col0)==0)
        {
            while((P1IN & Col0)==0);
            key=0;
        }
    }

    if((P1IN & Col1)==0)    //检测第1列
    {
        Delay10ms();
        if((P1IN & Col1)==0)
        {
            while((P1IN & Col1)==0);
            key=1;
        }
    }


    if((P1IN & Col2)==0)    //检测第2列
    {
        Delay10ms();
        if((P1IN & Col2)==0)
        {
            while((P1IN & Col2)==0);
            key=2;
        }
    }

    if((P1IN & Col3)==0)    //检测第3列
    {
        Delay10ms();
        if((P1IN & Col3)==0)
        {
            while((P1IN & Col3)==0);
            key=3;
        }
    }

    //扫描第1行
    P1OUT |= RowDis;   //所有管脚均置高
    P1OUT &= Row1;
    if((P1IN & Col0)==0)    //检测第0列
    {
        Delay10ms();
        if((P1IN & Col0)==0)
        {
            while((P1IN & Col0)==0);
            key=4;
        }
    }

    if((P1IN & Col1)==0)    //检测第1列
    {
        Delay10ms();
        if((P1IN & Col1)==0)
        {
            while((P1IN & Col1)==0);
            key=5;
        }
    }

    if((P1IN & Col2)==0)    //检测第2列
    {
        Delay10ms();
        if((P1IN & Col2)==0)
        {
            while((P1IN & Col2)==0);
            key=6;
        }
    }

    if((P1IN & Col3)==0)    //检测第3列
    {
        Delay10ms();
        if((P1IN & Col3)==0)
        {
            while((P1IN & Col3)==0);
            key=7;
        }
    }

    //扫描第2行
    P1OUT |= RowDis;   //所有管脚均置高
    P1OUT &= Row2;
    if((P1IN & Col0)==0)    //检测第0列
    {
        Delay10ms();
        if((P1IN & Col0)==0)
        {
            while((P1IN & Col0)==0);
            key=8;
        }
    }

    if((P1IN & Col1)==0)    //检测第1列
    {
        Delay10ms();
        if((P1IN & Col1)==0)
        {
            while((P1IN & Col1)==0);
            key=9;
        }
    }

    if((P1IN & Col2)==0)    //检测第2列
    {
        Delay10ms();
        if((P1IN & Col2)==0)
        {
            while((P1IN & Col2)==0);
            key=10;
        }
    }

    if((P1IN & Col3)==0)    //检测第3列
    {
        Delay10ms();
        if((P1IN & Col3)==0)
        {
            while((P1IN & Col3)==0);
            key=11;
        }
    }

    //扫描第3行
    P1OUT |= RowDis;   //所有管脚均置高
    P1OUT &= Row3;
    if((P1IN & Col0)==0)    //检测第0列
    {
        Delay10ms();
        if((P1IN & Col0)==0)
        {
            while((P1IN & Col0)==0);
            key=12;
        }
    }

    if((P1IN & Col1)==0)    //检测第1列
    {
        Delay10ms();
        if((P1IN & Col1)==0)
        {
            while((P1IN & Col1)==0);
            key=13;
        }
    }

    if((P1IN & Col2)==0)    //检测第2列
    {
        Delay10ms();
        if((P1IN & Col2)==0)
        {
            while((P1IN & Col2)==0);
            key=14;
        }
    }

    if((P1IN & Col3)==0)    //检测第3列
    {
        Delay10ms();
        if((P1IN & Col3)==0)
        {
            while((P1IN & Col3)==0);
            key=15;
        }
    }

    return key;
}


void Delay10ms()
{
    __delay_cycles(10000);  //10ms
}

