# UART通信简单应用

## 课程讲解内容

## 实验内容：使用USB转UART模块构建单片机与PC通信例程

### 实验目的：
#### 1、采用GRACE配置UART通信工作模式及管脚功能
#### 2、学习使用UART通信的基本使用方法

### 实验例程：
#### 1、MSP-LAB系统连线准备
    a、P1.1 与J30接插件上的TXD相连；（UART）
    b、P1.2 与J30接插件上的RXD相连；（UART）
    c、P2.0~P2.2 与独立按键的KEY1~KEY3相连；（三个按键输入）
    d、P3.0~P3.2 与DLEDA~DLEDC相连；（三个LED显示）
    e、确保DIGVCC与3.3V连接（LED缓冲芯片供电）

#### 2、GRACE生成代码的修改
    a、main主函数，添加while(1)循环；
    b、中断向量文件 InterruptVectors_init.c 中添加下面业务逻辑：
        i/ 函数：Delay_ms(unsigned short time) 函数（按键去抖用）
        ii/ Port2中断服务函数业务逻辑代码：发送不同的字符；
        iii/ UART接收中断服务函数业务逻辑代码：收到不同字符后对不同LED进行翻转；

#### 3、例程运行
    a、创建工程代码，关闭优化选项（Delay_ms函数有效），添加GRACE安装目录到include搜索路径；
    b、全速运行时，PC插入通信USB线，打开超级终端，分别发送字符a、b、c，会控制电路板上的三个LED的亮或者灭；电路板上分别按KEY1~KEY3，会在超级终端上收到字符'a' 'b' 'c'；

