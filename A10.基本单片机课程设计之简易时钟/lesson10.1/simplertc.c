#include <msp430.h>				


/**
 * 简易实时时钟
 * 从某固定时刻点开始计时，显示分 时-分-秒 年-月-日 切换
 */

// 延迟函数，毫秒为单位
void Delay_ms(int ms)
{
    int n;
    while(ms--)
    {
        // 每次循环大约10个clock，1MHz情况下，循环100次，为1000个clock，大约1ms
        // 注意需要确认工程设置中优化选项要关闭，否则程序会被忽略
        for(n = 100; n--; n>0)
            ;
    }
}

// 硬件连接
// P3 - 扫描数码管的段码
// P2 - 扫描数码管八个位码
// P1.1 - 按键，切换显示 时分秒 还是 年月日

// 八位数码管显示的内容缓存
unsigned long DispBuf = 0;

// 定义年月日的数据结构，使用C语言bit定义格式
struct date_s {
    unsigned char Day0 : 4;
    unsigned char Day1 : 4;
    unsigned char Mon0 : 4;
    unsigned char Mon1 : 4;
    unsigned char Year0 : 4;
    unsigned char Year1 : 4;
    unsigned char Year2 : 4;
    unsigned char Year3 : 4;
};

// 采用联合体定义，方便最终结果的赋值操作
union date_u {
    struct date_s Date_S;
    unsigned long Date_L;
} UsrDate;  // 全局变量定义

// 定义时分秒的数据结构，采用bit定义格式
struct time_s {
    unsigned char Sec0 : 4;
    unsigned char Sec1 : 4;
    unsigned char Mark0 : 4;
    unsigned char Min0 : 4;
    unsigned char Min1 : 4;
    unsigned char Mark1 : 4;
    unsigned char Hour0 : 4;
    unsigned char Hour1 : 4;
};

union time_u {
    struct time_s Time_S;
    unsigned long Time_L;
} UsrTime;  // 全局变量定义


// 显示模式，0 - 显示时分秒，1 - 显示年月日
unsigned char DispType = 0;

void main(void)
{
	WDTCTL = WDTPW | WDTHOLD;		// stop watchdog timer

	// P1.1作为按键输入，但不开启中断，切换显示模式
	P1SEL &= ~0x02;   //P11 as GPIO
	P1SEL2 &= ~0x02;  //P11 as GPIO
	P1DIR &= ~0x02;   //P11 as Input
	P1REN |= 0x02;  // 上下拉电阻使能
	P1OUT |= 0x02;  // 上拉电阻

	P2SEL = 0x00;   // P2 all as GPIO
	P2SEL2 = 0x00;  // P2 all as GPIO
	P2DIR = 0xFF;   // P2 all as Output
	P2OUT = 0xFF;   // All cube disable

	P3SEL = 0x00;   // P3 all as GPIO
	P3SEL2 = 0x00;  // P3 all as GPIO
	P3DIR = 0x0FF;  // P3 all as Output
	P3OUT = 0x00;   // All dot disable

	//TA0 作为扫描数码管刷新的时间计数
	TACCTL0 = CCIE;  // Timer 0 CCR0 Interrupt Enable
	TACCR0 = 249; // 1M/8/(499+1) = 125000/250 = 500 (2ms)
	// 尝试修改定时器时间，看看显示效果，以及思考如何计算
	TACTL = TASSEL_2 | ID_3 | MC_1 | TACLR;
	//      1MHz | Div 8 | Up Mode | Clear TAR

	//TA1 作为秒表时钟的计时时钟
	TA1CCTL0 = CCIE;
	TA1CCR0 = 62499;    // 500ms 计时时钟数
	TA1CTL = TASSEL_2 | ID_3 | MC_1 | TACLR;    //125KHz UpMode

	// 缺省值设置，其中A为中划线：2022年11月7日 9时37分49秒
	UsrTime.Time_L = 0x09A37A49;
	UsrDate.Date_L = 0x20221107;

	__enable_interrupt();  // 打开全局中断

	while(1)
	{
	    if(!(P1IN & 0x02))
	    {
	        Delay_ms(10);
	        if(!(P1IN & 0x02))
	        {
	            while(!(P1IN & 0x02))
	                ;
	            Delay_ms(10);
	            DispType ^= 0x01;   // 显示模式翻转
	        }
	    }
	}
}

// 共阳数码管字库，连接顺序（正序）：LEDDP - P27 ... LEDA - P20
//unsigned char DispLib[16] = {0xC0, 0xF9, 0xA4, 0xB0, 0x99, 0x92, 0x82, 0xF8, 0x80, 0x90, 0x88, 0x83, 0xC6, 0xA1, 0x86, 0x8E};
// 共阴数码管字库，连接顺序（正序）：LEDDP - P27 ... LEDA - P20，其中A~F均显示中划线
//const unsigned char DispLib[16] = {0x3F, 0x06, 0x5B, 0x4F, 0x66, 0x6D, 0x7D, 0x07, 0x7F, 0x6F, 0x77, 0x7C, 0x39, 0x5E, 0x79, 0x71};
const unsigned char DispLib[16] = {0x3F, 0x06, 0x5B, 0x4F, 0x66, 0x6D, 0x7D, 0x07, 0x7F, 0x6F, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40};     //0x77, 0x7C, 0x39, 0x5E, 0x79, 0x71};

#pragma vector = TIMER0_A0_VECTOR
__interrupt void Timer0_Isr(void)
{
    static unsigned char cnt = 0;
    unsigned char tmp;
    P2OUT = 0xFF;   // 所有数码管先熄灭，以免后面段码变化造成虚影现象
    // tmp 是取出当前要显示数字的那4个bit
    // (cnt << 2) 是每个数字占4bit，即(cnt * 4)，为了节省计算复杂度，采用(cnt<<2)
    tmp = (unsigned char)(DispBuf >> (cnt << 2));
    P3OUT = DispLib[tmp & 0x0F];    // 必须要屏蔽高4bit，所以采用tmp & 0x0F操作
    // 将对应要显示的数码管位置0，其余置1
    // (0x01 << cnt)是将对应位置一，其余为0
    P2OUT = ~(0x01 << cnt);

    // 准备下一位
    cnt ++;
    if(cnt > 7)
        cnt = 0;
}

#pragma vector = TIMER1_A0_VECTOR
__interrupt void Timer1_Isr(void)
{
    static unsigned cnt = 0;
    cnt ^= 0x01;
    if(cnt) // 每2次执行一次，正好为1S跳变一次
    {
        UsrTime.Time_S.Sec0 ++; //秒的个位数自增一
        if(UsrTime.Time_S.Sec0 > 9) // 个位进位操作
        {
            UsrTime.Time_S.Sec0 = 0;
            UsrTime.Time_S.Sec1 ++;
            if(UsrTime.Time_S.Sec1 > 5) // 超过59秒
            {
                UsrTime.Time_S.Sec1 = 0;
                UsrTime.Time_S.Min0 ++;
                if(UsrTime.Time_S.Min0 > 9) //个位进位操作
                {
                    UsrTime.Time_S.Min0 = 0;
                    UsrTime.Time_S.Min1 ++;
                    if(UsrTime.Time_S.Min1 > 5) // 超过59分
                    {
                        UsrTime.Time_S.Min1 = 0;
                        UsrTime.Time_S.Hour0 ++;
                        if(UsrTime.Time_S.Hour0 > 9)
                        {
                            UsrTime.Time_S.Hour0 = 0;
                            UsrTime.Time_S.Hour1 ++;
                        }
                        if( (UsrTime.Time_S.Hour1 >= 2) && (UsrTime.Time_S.Hour0 >= 4) )    //超过23时
                        {
                            UsrTime.Time_S.Hour0 = 0;
                            UsrTime.Time_S.Hour1 = 0;
                            UsrDate.Date_S.Day0 ++;
                            if(UsrDate.Date_S.Day0 > 9)
                            {
                                UsrDate.Date_S.Day0 = 0;
                                UsrDate.Date_S.Day1 ++;
                            }
                            if((UsrDate.Date_S.Day1 >= 3) && (UsrDate.Date_S.Day0 >= 2))    //暂时均以每个月最后一天为31号
                            {
                                UsrDate.Date_S.Day0 = 1;    //每个月从1号开始
                                UsrDate.Date_S.Day1 = 0;
                                UsrDate.Date_S.Mon0 ++;
                                if(UsrDate.Date_S.Mon0 > 9)
                                {
                                    UsrDate.Date_S.Mon0 = 0;
                                    UsrDate.Date_S.Mon1 ++;
                                }
                                if( (UsrDate.Date_S.Mon1 >= 1) && (UsrDate.Date_S.Mon0 >= 3))   //每年共1~12月
                                {
                                    UsrDate.Date_S.Mon0 = 1;    //每年从1月开始
                                    UsrDate.Date_S.Mon1 = 0;
                                    UsrDate.Date_S.Year0 ++;
                                    if(UsrDate.Date_S.Year0 > 9)
                                    {
                                        UsrDate.Date_S.Year0 = 0;
                                        UsrDate.Date_S.Year1 ++;
                                        if(UsrDate.Date_S.Year1 > 9)
                                        {
                                            UsrDate.Date_S.Year1 = 0;
                                            UsrDate.Date_S.Year2 ++;
                                            if(UsrDate.Date_S.Year2 > 9)
                                            {
                                                UsrDate.Date_S.Year2 = 0;
                                                UsrDate.Date_S.Year3 ++;
                                                if(UsrDate.Date_S.Year3 > 9)
                                                    UsrDate.Date_S.Year3 = 0;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if(DispType)    // 显示 年月日
            DispBuf = UsrDate.Date_L;
        else    // 显示 时分秒
            DispBuf = UsrTime.Time_L;
    }

}
