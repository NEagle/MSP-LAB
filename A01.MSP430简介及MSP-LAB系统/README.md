# MSP430简介及MSP-LAB系统

## 课程讲解内容

### MSP430系列简介

### MSP430G2553芯片介绍

### MSP-LAB实验系统介绍

## 实验内容：开发环境与下载运行验证

### 实验目的：
#### 1、安装开发工具
#### 2、创建工程并完成编译、下载、运行
#### 3、能够设置断点，开展单步调测，能够观察运行过程中各变量变化值

### 实验例程：
#### 1、MSP-LAB系统连线准备
    a、确保 仿真器部分与开发板部分的GND、3V3等电源线；SWD、SWK等仿真连接信号线的跳帽开关的连接；
    b、将接插件J24上的P1.0与接插件J29的PWM用杜邦线连接，验证LED闪烁；
    c、上述硬件修改检查正确后，插入USB数据线。

#### 2、例程全速运行现象
    a、LED以近似5Hz速率进行闪烁

#### 3、完成断点设置、单步运行、查看每次暂停后的P1OUT值的变化

