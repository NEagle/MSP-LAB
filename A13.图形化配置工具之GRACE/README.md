# 图形化配置工具 - GRACE 的使用

## 课程讲解内容

## 实验内容：采用GRACE工具配置第一个工程

### 实验目的：
#### 1、GRACE工具安装、创建工程、配置目标系统初始化程序；
#### 2、采用GRACE产生的代码，创建CCS工具

### 实验例程：
#### 1、MSP-LAB系统连线准备
    a、P1.0 与按键KEY1相连；
    b、P20~P27 与 段使能 DLEDA~DLEDDP 相连；
    c、P30~P37 与 位使能 DIG1~DIG8 相连；

#### 2、GRACE产生代码的业务逻辑添加
    a、main主程序，添加while(1)循环；
    b、中断向量文件中添加：
        i/ 函数：Delay_ms(unsigned short time) 函数
        ii/ 全局变量 DispLib、DispBuf
        iii/ Port1中断服务函数的业务逻辑（按键中断，修改DispBuf内容）
        iv/ Timer0_A0中断服务函数的业务逻辑（定时器中断，刷新数码管显示）

#### 3、例程运行
    a、需要注意，创建工程时，添加GRACE安装目录到include搜索路径；
    b、修改工程的优化选项，以使得Delay_ms()起效；
    c、全速运行，扫描数码管根据按键修改显示不同得内容（电话号码与年月日切换）
